# Hackathon „Neue Wege für die Mobilität in Augsburg“
## Our Team : GisJOE
1. Joe (Thunyathep Santhanavanich) [thunyathep.santhanavanich@hft-stuttgart.de]
2. Jan Silberer [jan.silberer@hft-stuttgart.de]
3. Verena Loidl [verena.loidl@hft-stuttgart.de]
4. Preston Rodrigues [preston.rodrigues@hft-stuttgart.de]

## Topics: "Happy Cycling"
A SMART ROUTING application created especially for cyclists. 
Cycling gives benefits in many ways including health, environment, economy, and traffic. The "Happy Cycling" application promotes people to cycling more by suggesting a happy route for them.

## Some idea from Matthias
Idea to implement an AI into our idea, in order to calculate the traffic flows of cars etc. - also to implement real time analysis for predicting traffic disruptions and increased passenger numbers

## Link to the Resources
* [conterra](https://www.conterra.de/hackathon)
* [Here](https://developer.here.com/faqs?create=Freemium-Basic&keepState=true&step=account#)