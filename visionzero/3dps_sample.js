var viewer = new Cesium.Viewer('cesiumContainer',{
    baseLayerPicker: false,
    imageryProvider : Cesium.createOpenStreetMapImageryProvider({})
});
viewer.scene.fog.enabled = false;
var igdcredit = new Cesium.Credit('Fraunhofer IGD', 'IGD.png', 'http://www.igd.fraunhofer.de/');
viewer.scene.frameState.creditDisplay.addDefaultCredit(igdcredit);
	
var oldstyling = null;
var dragging = undefined;
var drawing = false;
var handler = new Cesium.ScreenSpaceEventHandler(viewer.canvas);
var rectVisible = false;

function handleResponse(xml, bbox, layer) {
    var xmlDoc = xml.responseXML;
    // The url = Service Address of Provider
    var url = "http://tb13.igd.fraunhofer.de:8083" + xmlDoc.getElementsByTagName("address")[0].childNodes[0].nodeValue;
    loadTileset(url, bbox);

    document.getElementById("toolbar1").style.display = "inline";
	if (layer === "NY_LoD2"){
		document.getElementById("stylingBox").style.visibility = "hidden";
		oldstyling = null;
	}
	else if (layer === "berlin"){
		document.getElementById("stylingBox").style.visibility = "hidden";
		oldstyling = null;
	}
	else {
		document.getElementById("stylingBox").style.visibility = "visible";
	}
}

function requestGetCapabilities(){
	var requestUri = "http://tb13.igd.fraunhofer.de:8082/3dps?SERVICE=3DPS&ACCEPTVERSIONS=1.0&REQUEST=GetCapabilities"; 
    // Make request
    // Asynchron request
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {

        if (xhttp.readyState === 4 && xhttp.status === 200) {
			// get html element and add information
			var content = document.getElementById('window');
			content.innerHTML = '<h1 style="color:blue;">3DPS Capabilities </h1><textarea rows="30" cols="80" style="border:none;">' + xhttp.responseText + '</textarea>';
			var dialog = document.getElementById('dialog');
			dialog.style.visibility = "visible";
        }
        else if(xhttp.readyState === 4){
            console.log("Could not reach 3dps server.");
        }
    };
    xhttp.open("GET", requestUri, true);
    xhttp.send();

}

function createRequest(layer, bbox){
    // layer has to be "newyork" or "manhattan"
    // bbox has to be "west,south,east,north"
    // adress has to be adjusted
    var requestUri = "http://tb13.igd.fraunhofer.de:8082/3dps?SERVICE=3DPS&VERSION=1.0&REQUEST=GetScene&LAYERS="+ layer + "&FORMAT=text/html&CRS=EPSG:4326" + bbox; 
    // Make request
    // Asynchron request
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {

        if (xhttp.readyState === 4 && xhttp.status === 200) {
            handleResponse(xhttp, bbox, layer);
        }
        else if(xhttp.readyState === 4){
            console.log("Could not reach 3DPS server.");
        }
    };
    xhttp.open("GET", requestUri, true);
    xhttp.send();
}

///GUI

Sandcastle.addToolbarButton('Request Capabilities of 3DPS service', function() {
    requestGetCapabilities();
});

var currentLayer = "";
Sandcastle.addToolbarButton('Send 3DPS Request for New York', function() {
    currentLayer = "NY_LoD2";
    var bbox = "";
    createRequest(currentLayer, bbox);
}, "toolbar2");


Sandcastle.addToolbarButton('Send 3DPS Request for Manhattan', function() {
    currentLayer = "manhattan";
    var bbox = "";
    createRequest(currentLayer, bbox);
}, "toolbar2");

Sandcastle.addToolbarButton('Send 3DPS Request for Berlin', function() {
    currentLayer = "berlin";
    var bbox = "";
    createRequest(currentLayer, bbox);
}, "toolbar2");

Sandcastle.addToolbarButton('Remove data', function() {

    reset();
	document.getElementById("toolbar1").style.display = "none";
	
	//check visibility of rectangle
	if(rectVisible){
		toggleRectangleAndPins(false);
	}
	
}, "toolbar2");


Sandcastle.addToolbarButton('Draw BoundingBox', function() {
    rectVisible = true;
    drawing = false;
	toggleRectangleAndPins(true);
}, "toolbar3");

Sandcastle.addToolbarButton('Remove BoundingBox', function() {
	drawing = false;
    rectVisible = false;
	toggleRectangleAndPins(false);
}, "toolbar3");

Sandcastle.addToolbarButton('Send 3DPS Request for Bounding Box', function() {
    var degreeArray = getPinPositionsAsDegreeArray();
    var bbox = "&BOUNDINGBOX=" + String(degreeArray[0]) + ","+ String(degreeArray[1]) + "," + String(degreeArray[2]) + "," +String(degreeArray[3]);
    createRequest(currentLayer, bbox);
}, "toolbar3");


// load 3d tiles
var tileset;

function loadTileset(url, bbox) {
    reset();

    tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
        url : url,
        maximumNumberOfLoadedTiles : 3
    }));

    Cesium.when(tileset.readyPromise).then(function(tileset) {
		var degreeArray = getPinPositionsAsDegreeArray();
		tileset.maximumScreenSpaceError = 30;
		if(bbox === ""){
			var boundingSphere = tileset.boundingSphere;
			viewer.camera.flyToBoundingSphere(boundingSphere, new Cesium.HeadingPitchRange(0, -2.0, 0));
		}
		else{
			viewer.camera.flyTo({
				destination : Cesium.Rectangle.fromDegrees( degreeArray[0],degreeArray[1],degreeArray[2],degreeArray[3] )
			});
		}
		
		viewer.camera.lookAtTransform(Cesium.Matrix4.IDENTITY);
		
		var exp = "${Longitude} >= "+ degreeArray[0]+" && ${Latitude} >= "+ degreeArray[1] +" && ${Longitude} <= "+ degreeArray[2]+" && ${Latitude} <= "+ degreeArray[3];
		
		if(bbox !== ""){
			tileset.style = new Cesium.Cesium3DTileStyle(
				{ 
					show : exp
				}
		);
	}
	});
}

function reset() {
    if (Cesium.defined(tileset)) {
        viewer.scene.primitives.remove(tileset);
    }
}

                                                  
//initial camera 
var camRect = Cesium.Rectangle.fromDegrees( -74.2554824,40.498434755378184,-73.700049137171745883,40.915055674909346806);
viewer.camera.setView({
        destination: camRect
    });
    
///Adding heat Style Theme
Sandcastle.addToolbarButton('Heat theme on/off', function()
 {
		if(oldstyling === null){
		oldstyling = tileset.style;
            var filtervalue ="heat";
		tileset.style = new Cesium.Cesium3DTileStyle(
			{           
			color : {
				conditions : [
				["(${heat} <= 50.0)" , "color('#6495ED')"],
					["(${heat} >= 50.0) && (${heat} < 70.0)" , "color('#008000')"],
					["(${heat} >= 70.0) && (${heat} < 80.0)" , "color('#67B200')"],
					["(${heat} >= 80.0) && (${heat} < 100.0) " , "color('#86E307')"],
					["(${heat} >= 100.0) && (${heat} < 110.0) " , "color('#B1E309')"],
					["(${heat} >= 110.0) && (${heat} < 120.0) " , "color('#FFFF00')"],
					["(${heat} >= 120.0) && (${heat} < 130.0) " , "color('#E2BF0C')"],
					["(${heat} >= 130.0) && (${heat} < 150.0) " , "color('#FF8000')"],
					["(${heat} >= 150.0)  " , "color('#FF0000')"],
					["true" , "color('#666666')"]
				]}
		});
	}
	else{
		tileset.style = oldstyling;
		oldstyling = null;
	}
	
}, 'toolbar4');
 
 

 
 // show meta information on click
 
handler.setInputAction(function(movement) {
	var building = viewer.scene.pick(movement.position);
	
    if (Cesium.defined(building)) {
        
            var properties = building.primitive.properties; // get properties from tileset
			console.log(properties);
            if (Cesium.defined(properties)) {
                var id = building.getProperty('Building ID');
			
				var request = 'http://81.169.187.7/api/v2/endpoint?service=3DPS&version=1.0&request=GetFeatureInfoByObjectId&objectid='+id;
                
				//create html information panel
				var textToDisplay = "<h1 style='color:blue;'>3DPS GetFeatureByID</h1><h3 style='color:black;'>Request</h3>";
				textToDisplay += "<p>" + request + "</p>";
				textToDisplay += "<h3 style='color:black;'>Feature Details</h3>";
				textToDisplay +="<p>OBJECTID : "+ building.getProperty('OBJECTID')+"<p>";
				textToDisplay +="<p>Heat : "+ building.getProperty('heat')+"<p>";
				textToDisplay +="<p>MeasuredHeight : "+ building.getProperty('MeasuredHeight')+"<p>";
				
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if (xhttp.readyState === 4 && xhttp.status === 200) {
						//extract information from service's response
						var buildingArray = JSON.parse(xhttp.responseText).attributes[0];
						var addr = buildingArray.pluto_lot_address;
						var vol = buildingArray.building_volume;
						var year = buildingArray.pluto_year_built;
						textToDisplay+="<p>Address : "+ String(addr)+"<p>";
						textToDisplay+="<p>Volume : "+ String(vol)+"<p>";
						textToDisplay+="<p>Year : "+ String(year)+"<p>";
					
						// get html element and add information
						var content = document.getElementById('window');
						content.innerHTML = textToDisplay;
						var dialog = document.getElementById('dialog');
						dialog.style.visibility = "visible";
					}
					else if(xhttp.readyState === 4){
						console.log("Could not reach 3DPS GET RESSOURCE BY ID SERVICE.");
					}
				};
				console.log(request);

				xhttp.open("GET", request, true);
				xhttp.send();
            }
    }
}, Cesium.ScreenSpaceEventType.LEFT_CLICK);
 



var pinBuilder = new Cesium.PinBuilder();

var pin1 = viewer.entities.add({
    position: Cesium.Cartesian3.fromDegrees(74.018997336757550443,40.69846622682742776, 0.0),
    billboard: {
        image: new Cesium.ConstantProperty(pinBuilder.fromText("1",Cesium.Color.BLUE, 48)),
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        show: false
    }
});

var pin2 = viewer.entities.add({
    position: Cesium.Cartesian3.fromDegrees(73.969593360166612683,40.758282705492597131, 0.0),
    billboard: {
        image: new Cesium.ConstantProperty(pinBuilder.fromText("2",Cesium.Color.BLUE, 48)),
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        show: false
    }
});


var bbRectangle = viewer.entities.add({
    rectangle : {
        coordinates : new Cesium.CallbackProperty(getPinPositions, false),
        material : Cesium.Color.BLUE.withAlpha(0.2),
        outline : true,
        outlineColor : Cesium.Color.WHITE,
        extrudedHeight : 2,
        show : false
    }
});

function getPinPositionsAsDegreeArray() {
    var carto1 = Cesium.Ellipsoid.WGS84.cartesianToCartographic(pin1.position.getValue(0));     
    var carto2 = Cesium.Ellipsoid.WGS84.cartesianToCartographic(pin2.position.getValue(0));     
    var lon1 = Cesium.Math.toDegrees(carto1.longitude); 
    var lat1 = Cesium.Math.toDegrees(carto1.latitude); 
    
    var lon2 = Cesium.Math.toDegrees(carto2.longitude); 
    var lat2 = Cesium.Math.toDegrees(carto2.latitude); 
    
    var smallerLat =0;
    var biggerLat =0;
    if (lat1< lat2) {
        smallerLat = lat1; biggerLat = lat2;
    }else{
        smallerLat = lat2; biggerLat = lat1;
    }
    var smallerLon =0;
    var biggerLon =0;
    if (lon1< lon2) {
        smallerLon = lon1; biggerLon = lon2;
    }
    else{
        smallerLon = lon2; biggerLon = lon1;
    }
    return [smallerLon, smallerLat ,biggerLon,biggerLat];
}

function getPinPositions() {
   var degreeArray = getPinPositionsAsDegreeArray();
   var rectangle = Cesium.Rectangle.fromDegrees( degreeArray[0],degreeArray[1],degreeArray[2],degreeArray[3] );
   return rectangle;
}



function toggleRectangleAndPins(show){
    pin1.billboard.show = show;
    pin2.billboard.show = show;
    bbRectangle.rectangle.show = show;
}

handler.setInputAction(function(click) {
	if(rectVisible){
		drawing = true;
		var position = getMousePosition(click.position);
		pin1.position._value = position;
        viewer.scene.screenSpaceCameraController.enableRotate = false;
	}
}, Cesium.ScreenSpaceEventType.LEFT_DOWN);

handler.setInputAction(function() {
    if(rectVisible && drawing){

        drawing = false;
        rectVisible = false;
        viewer.scene.screenSpaceCameraController.enableRotate = true;
        
    }

}, Cesium.ScreenSpaceEventType.LEFT_UP);

// drag PINs
handler.setInputAction(function(movement) {
    if(rectVisible){
        var position = getMousePosition(movement.endPosition);
        if(position !== -1){
            pin2.position._value = position;
            if(!drawing){
				pin1.position._value = position;
            }
        }
    }
}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

function getMousePosition(mousePosition){
	var ray = viewer.camera.getPickRay(mousePosition);
    var position = viewer.scene.globe.pick(ray, viewer.scene);
    if (!Cesium.defined(position) || !(dragging !== undefined || rectVisible === true)){
		return -1;
	}
	return position;
}	

