//////////////////////////////////
// A Function to trigger the menu////
//////////////////////////////////
var openRTEI = function () {
    if (RTEI.style.display === 'none') {
        // RTEI.style.display = "block";
        // HEU.style.display = "none";
        // DrawBB.style.display = "none";
        // MapP.style.display = "none";
        // CamMe.style.display = "none";
        // $("#RTEI").show("1000");
        // $("#HEU").hide("1000");
        // $("#DrawBB").hide("1000");
        $("#MapP").hide("1000");
        // $("#CamMe").hide("1000");
    } else {
        //RTEI.style.display = "none";
        $("#RTEI").hide("1000");
    }

}
var openHEU = function () {
    if (HEU.style.display === 'none') {
        $("#RTEI").hide("1000");
        $("#HEU").show("1000");
        // $("#DrawBB").hide("1000");
        $("#MapP").hide("1000");
        $("#CamMe").hide("1000");
    } else {
        $("#HEU").hide("1000");
    }
}
var openStat = function () {
    if (CamMe.style.display === 'none') {
        $("#RTEI").hide("1000");
        $("#HEU").hide("1000");
        $("#DrawBB").hide("1000");
        $("#MapP").hide("1000");
        $("#CamMe").show("1000");
    } else {
        $("#CamMe").hide("1000");
    }
}
var open3DrawBB = function () {
    if (DrawBB.style.display === 'none') {
        // $("#RTEI").hide("1000");
        // $("#HEU").hide("1000");
        // $("#DrawBB").show("1000");
        $("#MapP").hide("1000");
        // $("#CamMe").hide("1000");
    } else {
        $("#DrawBB").hide("1000");
    }
}
var openMapP = function () {
    if (MapP.style.display === 'none') {
        // $("#RTEI").hide("1000");
        // $("#HEU").hide("1000");
        // $("#DrawBB").hide("1000");
        $("#MapP").show("1500");
        $("#menuCircleUp").show();
        $("#menuCircleDown").hide();

        // $("#CamMe").hide("1000");
    } else {
        $("#MapP").hide("1500");
        $("#menuCircleUp").hide();
        $("#menuCircleDown").show();
    }
}
var displayMenu = function () {
    if (menu.style.display === 'none') {
        //menu.style.display = "block";
        $("#menu").fadeIn("3000");
    } else {
        //menu.style.display = "none"
        $("#menu").fadeOut("3000");
    }
}
var hideCapability = function () {
    $("#CapPopup").hide("1500");
}
var remarkActivate = function () {
    if (Hints.style.display === 'none') {
        $("#Hints").show("1000");
        $("#remarkCircleUp").show();
        $("#remarkCircleDown").hide();
    } else {
        $("#Hints").hide("1000");
        $("#remarkCircleDown").show();
        $("#remarkCircleUp").hide();
    }
}
var routingActivate = function () {
    if (routingpart.style.display === 'none') {
        $("#routingpart").show("1000");
        $("#routingCircleUp").show();
        $("#routingCircleDown").hide();
    } else {
        $("#routingpart").hide("1000");
        $("#routingCircleDown").show();
        $("#routingCircleUp").hide();
    }
}
var layerActivate = function () {
    if (layerpart.style.display === 'none') {
        $("#layerpart").show("1000");
        $("#layerCircleUp").show();
        $("#layerCircleDown").hide();
    } else {
        $("#layerpart").hide("1000");
        $("#layerCircleDown").show();
        $("#layerCircleUp").hide();
    }
}
$(document).ready(function () {
    $('#Cesium3DTilesCheck').on('change', function () {

        if ($("#Cesium3DTilesCheck").prop("checked") == true) {
            // addLayers3DT();
            tileset.show = true;
            console.log("3D-Tile: Check true");
        } else {
            tileset.show = false;
            console.log("3D-Tile: Check false");
        }
    });
    $('#Slider_HeatMap').on('change', function () {

        if ($("#Slider_HeatMap").prop("checked") == true) {
            heatmapPolygon.show = true;
            console.log("Slider_HeatMap: Check true");
        } else {
            heatmapPolygon.show = false;
            console.log("Slider_HeatMap: Check false");
        }
    });
    $('#Slider_grid').on('change', function () {
        if ($("#Slider_grid").prop("checked") == true) {
            fishnetPolygon.show = true;
            // loadFishnet();
            console.log("Slider_grid: Check true");
        } else {
            console.log("Slider_grid: Check false");
            fishnetPolygon.show = false;
        }
    });
    
    $('#Styling_Option').on('change', function () {
        var selectedStyle = Styling_Option.options[Styling_Option.selectedIndex].value;
        if (selectedStyle === "Styling_Option_simple") {
            $("#MeasuredHeight_Style_Legend").hide("1000");
            $("#MeasuredHeat_Style_Legend").hide("1000");
            $("#MeasuredBuildingArea_Style_Legend").hide("1000");
            TilesetStyle_Simple();
            console.log("Simple style selected");
        } else if (selectedStyle === "Styling_Option_MHeight") {
            $("#MeasuredHeight_Style_Legend").show("1000");
            $("#MeasuredBuildingArea_Style_Legend").hide("1000");
            $("#MeasuredHeat_Style_Legend").hide("1000");
            TilesetStyle_Height();
            console.log("style by height selected");
        } else if (selectedStyle === "Styling_Option_Heat") {
            $("#MeasuredHeight_Style_Legend").hide("1000");
            $("#MeasuredHeat_Style_Legend").show("1000");
            $("#MeasuredBuildingArea_Style_Legend").hide("1000");

            alert("This feature will be available soon!")
            // TilesetStyle_Heat();
            console.log("style by heat selected");
        } else if (selectedStyle === "Styling_Option_BuildingArea") {
            $("#MeasuredBuildingArea_Style_Legend").show("1000");
            $("#MeasuredHeight_Style_Legend").hide("1000");
            $("#MeasuredHeat_Style_Legend").hide("1000");
            // alert("This feature will be available soon!");
            // TilesetStyle_Heat();
            TilesetStyle_PLUTO_building_area();
            console.log("style by Building Area selected");
        }

    });

});

$(function () {
    var handle = $("#custom-handle");
    $("#TransparentSlider").slider({
        create: function () {
            handle.text($(this).slider("value") + "%");
        },
        slide: function (event, ui) {
            handle.text(ui.value + "%");
            var transValue = 1 - (ui.value / 100);
            var selectedStyle = Styling_Option.options[Styling_Option.selectedIndex].value;
            if (Cesium.defined(tileset)) {
                if (selectedStyle === "Styling_Option_simple") {
                    var transparentStyle = new Cesium.Cesium3DTileStyle({
                        color: "color('WHITE'," +transValue+")",
                        show: true
                    });
                    tileset.style = transparentStyle;
                    console.log("transparency change")
                }
                if (selectedStyle === "Styling_Option_MHeight") {
                    console.log("transparency change")
                    var heightStyle = new Cesium.Cesium3DTileStyle({
                        color: {
                            conditions: [
                                ["${MeasuredHeight} >= 300", "rgba(45, 0, 75," + transValue + ")"],
                                ["${MeasuredHeight} >= 200", "rgba(102, 71, 151," + transValue + ")"],
                                ["${MeasuredHeight} >= 100", "rgba(170, 162, 204," + transValue + ")"],
                                ["${MeasuredHeight} >= 50", "rgba(224, 226, 238," + transValue + ")"],
                                ["${MeasuredHeight} >= 25", "rgba(252, 230, 200," + transValue + ")"],
                                ["${MeasuredHeight} >= 10", "rgba(248, 176, 87," + transValue + ")"],
                                ["${MeasuredHeight} >= 5", "rgba(198, 106, 11," + transValue + ")"],
                                ["true", "rgba(127, 59, 8," + transValue + ")"]
                            ]
                        },
                        meta: {
                            description: '"Building id ${id} has height ${Height}."'
                        }
                    });
                    tileset.style = heightStyle;
                }
                if (selectedStyle === "Styling_Option_BuildingArea") {
                    console.log("transparency change")
                    var buildingAreaStyle = new Cesium.Cesium3DTileStyle({
                        color: {
                            conditions: [
                                // ["(${PLUTO_building_area} === '')", "color('#6495ED')"],
                                ["(${PLUTO_building_area} >= 300000.0)  ", "rgba(255,0,0," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 150000.0)", "rgba(255,128,0," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 75000.0) ", "rgba(226,191,12," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 37500.0) ", "rgba(255,255,0," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 18000.0) ", "rgba(177,227,9," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 9000.0)", "rgba(0,128,0," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 4500.0)", "rgba(134,227,7," + transValue + ")"],
                                ["(${PLUTO_building_area} >= 2000.0)", "rgba(103,178,0," + transValue + ")"],
                                ["true", "rgba(103,178,0," + transValue + ")"]
                            ]
                        },
                        meta: {
                            description: '"Building id ${id} has height ${Height}."'
                        }
                    });
                    tileset.style = buildingAreaStyle;
                }
            }
        }
    });
});
// $(document).ready(function () {
//     $('#Cesium3DTilesCheck').on('change', function () {

//         if ($("#Cesium3DTilesCheck").prop("checked") == true) {
//             addLayers3DT();
//             console.log("3D-Tile: Check true");
//         } else {
//             TurnOff3DT();
//             console.log("3D-Tile: Check false");
//         }
//     });
//     $('#RealtimeEbikeSlider').on('change', function () {
//         if ($("#RealtimeEbikeSlider").prop("checked") == true) {
//             ShowEbike()
//             // console.log("label Check true");
//         } else {
//             HideEbike();
//             // console.log("label Check false");
//         }
//     });
//     $('#Slider_MappinEbike').on('change', function () {
//         if ($("#Slider_MappinEbike").prop("checked") == true) {
//             ShowMapPinEbike()
//             // console.log("label Check true");
//         } else {
//             HideMapPinEbike();
//             // console.log("label Check false");
//         }
//     });
//     $('#Slider_ShowBikeRental').on('change', function () {
//         if ($("#Slider_ShowBikeRental").prop("checked") == true) {
//             ShowBikeRental()
//             // console.log("label Check true");
//         } else {
//             HideBikeRental();
//             // console.log("label Check false");
//         }
//     });
//     $('#Slider_ShowMapPinPublic').on('change', function () {
//         if ($("#Slider_ShowMapPinPublic").prop("checked") == true) {
//             ShowMapPinPublic()
//             // console.log("label Check true");
//         } else {
//             HideMapPinPublic();
//             // console.log("label Check false");
//         }
//     });
//     $('#Slider_ShowMapPinCarsharing').on('change', function () {
//         if ($("#Slider_ShowMapPinCarsharing").prop("checked") == true) {
//             ShowMapPinCarsharing()
//             console.log("label Check true");
//         } else {
//             HideMapPinCarsharing();
//             console.log("label Check false");
//         }
//     });

// });