var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
var showpo = false;
var showhint = false;
// a function to get the click location
var getLocationFrom = false;
var getLocationTo = false;
var getLocaFrom = function () {
    entity_showHint.label.show = true;
    showhint = true;
    viewer.entities.remove(RountPinPoint_A);
    getLocationFrom = true;
}
var getLocaTo = function () {
    entity_showHint.label.show = true;
    showhint = true;
    viewer.entities.remove(RountPinPoint_B);
    getLocationTo = true;
}

var longitudeString, latitudeString;
var RountPinPoint_A, RountPinPoint_B;

//function to show mouse move on Location
var entity_showLatLon = viewer.entities.add({
    label: {
        show: false,
        showBackground: true,
        font: '14px monospace',
        horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
        verticalOrigin: Cesium.VerticalOrigin.TOP,
        pixelOffset: new Cesium.Cartesian2(15, 0),
        disableDepthTestDistance: "999999"
    }
});
var entity_showHint = viewer.entities.add({
    label: {
        text: "Right click to confirm position",
        show: false,
        showBackground: true,
        font: '14px monospace',
        horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
        verticalOrigin: Cesium.VerticalOrigin.TOP,
        pixelOffset: new Cesium.Cartesian2(15, 0),
        disableDepthTestDistance: "999999"
    }
});
// Mouse over the globe to see the cartographic position
handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
handler.setInputAction(function (movement) {
    var cartesian = viewer.camera.pickEllipsoid(movement.endPosition, viewer.scene.globe.ellipsoid);
    var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
    longitudeString = Cesium.Math.toDegrees(cartographic.longitude);
    latitudeString = Cesium.Math.toDegrees(cartographic.latitude);
    if (getLocationFrom) {
        document.getElementById('RouteFromLong').value = longitudeString
        document.getElementById('RouteFromLat').value = latitudeString
    }
    if (getLocationTo) {
        document.getElementById('RouteToLong').value = longitudeString
        document.getElementById('RouteToLat').value = latitudeString
    }
    entity_showLatLon.position = cartesian;
    entity_showHint.position = cartesian;
    // entity_showLatLon.label.show = true;
    entity_showLatLon.label.text =
        'Lat: ' + ('   ' + latitudeString) + '\u00B0' +
        '\nLon: ' + ('   ' + longitudeString) + '\u00B0';
    handler.setInputAction(function (click) {
        if (viewer.scene.pickPositionSupported && Cesium.defined(cartesian) && getLocationFrom) {
            document.getElementById('RouteFromLong').value = longitudeString;
            document.getElementById('RouteFromLat').value = latitudeString;
            RountPinPoint_A = viewer.entities.add({
                name: "A",
                position: Cesium.Cartesian3.fromDegrees(longitudeString, latitudeString),
                description: "Starting Point",
                billboard: {
                    image: pinBuilder.fromText('A', Cesium.Color.SEAGREEN, 60),
                    verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                    heightReference: "CLAMP_TO_GROUND",
                    show: true,
                    disableDepthTestDistance: "999999"
                }
            });
            entity_showHint.label.show = false;
            showhint = false;
            getLocationFrom = false;
        } else if (viewer.scene.pickPositionSupported && Cesium.defined(cartesian) && getLocationTo) {
            document.getElementById('RouteToLong').value = longitudeString;
            document.getElementById('RouteToLat').value = latitudeString;
            RountPinPoint_B = viewer.entities.add({
                name: "B",
                position: Cesium.Cartesian3.fromDegrees(longitudeString, latitudeString),
                description: "Starting Point",
                billboard: {
                    image: pinBuilder.fromText('B', Cesium.Color.MEDIUMSLATEBLUE, 60),
                    verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                    heightReference: "CLAMP_TO_GROUND",
                    show: true,
                    disableDepthTestDistance: "999999"
                }
            });
            entity_showHint.label.show = false;
            showhint = false;
            getLocationTo = false;
        }
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

var openShowLo = function () {
    if (!showpo) {
        showpo = true;
        entity_showLatLon.label.show = true;
    } else if (showpo) {
        showpo = false;
        entity_showLatLon.label.show = false;

    }
}