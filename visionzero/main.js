// Cesium.BingMapsApi.defaultKey = 'ApOW9LMkerqWIVSnFauilSeaZyp8df66byy1USCTjgTdMvhb4y1iAhEsUHQfCgzq'
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOGQ1ODcwNC0zNDU1LTQ5ZGEtOWE4YS0zYzNmYTcxOWQxYTUiLCJpZCI6MTAxMywiaWF0IjoxNTI2NjM1Nzg4fQ.X43-3OHqmcwPP5qCE2NqSGJUsj5FGWgXAzitvrV0pwY';

var viewer = new Cesium.Viewer('cesiumContainer', {
    // terrainProvider: Cesium.createWorldTerrain(),
    // imageryProvider: Cesium.createOpenStreetMapImageryProvider({
    //     url: 'https://a.tile.openstreetmap.org/'
    // }), //use 2d instead of 3d
    baseLayerPicker: true,
    homeButton: true,
    // scene3DOnly: true,
    //terrainExaggeration: 1.5,
    baseLayerPicker : true,
    navigationHelpButton: false,
    selectionIndicator: false,
    sceneModePicker: false,
    geocoder: false,
    shadows: false,
    // imageryProvider : new Cesium.ArcGisMapServerImageryProvider({
    //     url : '//services.arcgis.com/1lplwYilIlo008hQ/arcgis/rest/services/AccidentAugburgSHP_Density/MapServer'
        
    // }),
    // imageryProvider : new Cesium.IonImageryProvider({ assetId: 4 })
});
// Basic Set Up for the Cesium Globe
viewer.scene.globe.enableLighting = true;
//initial camera 
var homeCameraView = {
    destination: {x: 4170968.13780109, y: 802446.9273112626, z: 4743438.883970183},
        orientation: {
            direction: {x: -0.9529061049644411, y: -0.16277410459063243, z: 0.2558799444978361},
            up: {x: 0.24956343558603447, y: 0.05848955415010799, z: 0.966590432227537}
        }
};
// Add some camera flight animation options
homeCameraView.duration = 2.0;
homeCameraView.maximumHeight = 2000;
homeCameraView.pitchAdjustHeight = 2000;
homeCameraView.endTransform = Cesium.Matrix4.IDENTITY;
viewer.scene.camera.setView(homeCameraView);
viewer.homeButton.viewModel.command.beforeExecute.addEventListener(function (e) {
    e.cancel = true;
    viewer.scene.camera.flyTo(homeCameraView);
});
viewer.clock.shouldAnimate = true; // default00
viewer.clock.startTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.stopTime = Cesium.JulianDate.fromIso8601("2018-11-24T18:00:00Z");
viewer.clock.currentTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.multiplier = 0.5; // sets a speedup
viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP; // loop at the end
viewer.timeline.zoomTo(viewer.clock.startTime, viewer.clock.stopTime); // set visible range

//Function to fly somewhere, in this case, New York
var flyNY = function () {
    //NY Area
    viewer.camera.flyTo({
        destination: {x: 4179188.763664804, y: 808050.9602644673, z: 4743944.473377325},
        orientation: {
            direction: {x: -0.8681675702516746, y: -0.4944856780032176, z: -0.04205929401454769},
            up: {x: 0.10674035840875822, y: -0.2688248601567101, z: 0.957256334765404}
        }
    });
}
var tileset;
var addLayers3DT = function () {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/AugsburgClampedOutput2/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    // Adjust a tileset's height from the globe's surface.
    // tileset.readyPromise.then(function () {
    //     var heightOffset = 150;
    //     var boundingSphere = tileset.boundingSphere;
    //     var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
    //     var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
    //     var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
    //     var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
    //     tileset.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    // });
    // tilesetRoof.readyPromise.then(function () {
    //     var heightOffset = 50;
    //     var boundingSphere = tilesetRoof.boundingSphere;
    //     var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
    //     var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
    //     var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
    //     var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
    //     tilesetRoof.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    // });
    
    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('WHITE', 1)",
        show: true
    });
    tileset.style = transparentStyle;
};


addLayers3DT();
