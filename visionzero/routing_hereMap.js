var routeServiceHereAnswer;
var routeManuver;
var HerePositionArray = [];
var czmlRoute,routeDatasource;
var routeServiceHere_ii = 0;
var mode = "pedestrian";
var parkmode = "park:0";
// var modeselected = routingModeSelected.options[routingModeSelected.selectedIndex].value; 
$('#routingModeSelected').on('change', function() {
    console.log( "Mode:" + this.value +"is selected!" );
    mode = this.value;
    if (this.value == "pedestrian") {
        parkmode = "park:0";
    } else if (this.value == "bicycle") {
        parkmode = "park:0";
    }
  })
var routeServiceHere = function () {
    // viewer.dataSources.removeAll();
    const api_id = '6LZYFSnYejL8kWG2axRV';
    const app_code = '5SBQQYZiHcucej66-tMyBw';
    var fromPo = [document.getElementById('RouteFromLong').value, document.getElementById('RouteFromLat').value];
    var toPo = [document.getElementById('RouteToLong').value, document.getElementById('RouteToLat').value];
    var URL_here = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=6LZYFSnYejL8kWG2axRV&app_code=5SBQQYZiHcucej66-tMyBw&waypoint0=geo!"+fromPo[1]+","+fromPo[0]+"&waypoint1=geo!"+toPo[1]+","+toPo[0]+"&mode=fastest;"+mode+";traffic:disabled;" + parkmode;
    console.log("Request routing at ... " + URL_here);
    $.getJSON(URL_here, function (hereAnswer) {
        console.log( "request route success" );
        routeServiceHereAnswer = hereAnswer;
        routeManuver = routeServiceHereAnswer.response.route[0].leg[0].maneuver;
        var travelTime = 0
        for (let i = 0; i < routeManuver.length; i++) {
            // console.log("lat" + i + ":" + routeManuver[i].position.latitude);
            // console.log("lon" + i + ":" + routeManuver[i].position.longitude);
            HerePositionArray.push(travelTime,routeManuver[i].position.longitude,routeManuver[i].position.latitude,10);
            travelTime += routeManuver[i].travelTime;
        }
        
        var StartTime = new Date().toISOString();
        // var StartTime = "2019-10-15T12:00:00.000Z"; // uncomment if want to fix the starting time
        var StartTimeParse = Date.parse(StartTime);
        var EntTimeParse = StartTimeParse + travelTime*1000;
        var EndTime = new Date(EntTimeParse).toISOString();
        console.log("StartTime: " + StartTime );
        console.log("EndTime: " + EndTime );
        czmlRoute = buildRoad(StartTime,EndTime,HerePositionArray);
        routePromise = Cesium.CzmlDataSource.load(czmlRoute);
        routePromise.then(function (dataSource) {
            console.log("Load CZML!")
            viewer.dataSources.add(dataSource);
            routeDatasource = dataSource.entities.getById("Routing");
        });
    });
}
function buildRoad (startTime,endTime,LocationData) {
    var result = [{
        "id": "document",
        "name": "Data",
        "version": "1.0",
        "clock": {
            "interval": startTime + "/" + endTime,
            "currentTime": startTime,
            "multiplier": 2,
            "range": "LOOP_STOP",
            "step": "SYSTEM_CLOCK_MULTIPLIER"
        }
    }, {
        "id": "Routing",
        "name": "ebike 1 with Garmin",
        "availability": startTime + "/" + endTime,
        "ellipsoid": {
            "radii": {
                "cartesian": [5, 5, 5]
            },
            "fill": true,
            "material": {
                "solidColor": {
                    "color": {
                        "rgba": [255, 0, 0, 255]
                    }
                }
            }
        },
        // "label": {
        //     "text": "Routing",
        //     "font": "14pt sans-serif",
        //     "heightReference": "CLAMP_TO_GROUND",
        //     "showBackground": "true",
        //     "horizontalOrigin": "LEFT",
        //     "verticalOrigin": "BASELINE",
        //     "backgroundPadding": {
        //         "cartesian2": [20, 8]
        //     },
        //     "pixelOffset": {
        //         "cartesian2": [50, -50]
        //     },
        //     "backgroundColor": {
        //         "rgba": [
        //             0,
        //             0,
        //             0,
        //             125
        //         ]
        //     },
        //     "disableDepthTestDistance": 999999
        // },
        "path": {
            "show": [{
                "interval": startTime + "/" + endTime,
                "boolean": true
            }
            ],
            "width": 4,
            "material": {
                "polylineOutline": {
                    "color": {
                        "rgba": [0, 255, 0, 255]
                    },
                    "outlineColor": {
                        "rgba": [0, 0, 0, 255]
                    },
                    "outlineWidth": 1
                }
            },
            "resolution": 1200,
            "leadTime": 0
        },
        "position": {
            "interpolationAlgorithm": "LINEAR",
            "interpolationDegree": 1,
            "epoch": startTime,
            "cartographicDegrees": LocationData
        }
    }];
    return result
}
